(function() {
var toc =  [{"type":"item","name":"Creating Tokens","url":"Creating_Tokens.htm"},{"type":"item","name":"Forecasting","url":"Forecasting.htm"},{"type":"item","name":"Learning","url":"Learning.htm"},{"type":"item","name":"Scoring","url":"Scoring.htm"},{"type":"item","name":"Simulating","url":"Simulating.htm"}];
window.rh.model.publish(rh.consts('KEY_TEMP_DATA'), toc, { sync:true });
})();