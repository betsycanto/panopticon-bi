(function() {
var toc =  [{"type":"item","name":"Connecting to Web Services","url":"Connecting_to_Web_Service.htm"},{"type":"item","name":"Connecting to Candi Controls","url":"Connecting_to_Candi_Controls.htm"},{"type":"item","name":"Connecting to MQTT","url":"Connecting_to_MQTT.htm"},{"type":"item","name":"Connecting to WebSocket","url":"Connecting_to_WebSocket.htm"}];
window.rh.model.publish(rh.consts('KEY_TEMP_DATA'), toc, { sync:true });
})();