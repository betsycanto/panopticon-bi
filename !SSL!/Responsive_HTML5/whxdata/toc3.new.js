(function() {
var toc =  [{"type":"item","name":"Getting Started with the Altair Panopticon BI Workspace","url":"First_Topic.htm"},{"type":"item","name":"Data Sources pane","url":"Data_Source_window.htm"},{"type":"item","name":"Dimensions pane","url":"Dimensions_pane.htm"},{"type":"item","name":"Tool bar","url":"Toolbar.htm"},{"type":"item","name":"Filters and Settings pane","url":"Filters_and_Settings_pane.htm"}];
window.rh.model.publish(rh.consts('KEY_TEMP_DATA'), toc, { sync:true });
})();