(function() {
var toc =  [{"type":"item","name":"Connecting to Databases","url":"Connecting_to_data_sources_(Oracle_and_other_DBs).htm"},{"type":"item","name":"Connecting to Data Engines","url":"Connecting_to_Data_Engines.htm"}];
window.rh.model.publish(rh.consts('KEY_TEMP_DATA'), toc, { sync:true });
})();